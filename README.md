# UniprotPPIreproducible

This project aims at retrieving the proteins that interact with a protein of interest from UniProt using SPARQL (e.g. for INSR_HUMAN ([P06213](https://www.uniprot.org/uniprotkb/P06213)) below).

This project is a companion to [uniprotPPI](https://gitlab.com/odameron/uniprotPPI) which was focused on the exploration of the UniProt's data model.

Section 1 consists in securing a convenient access to the data that will be used in the next sections.
Section 2 then focuses on retrieving the interaction network.
Section 3 generates ad-hoc representation of this network (visual, as a RDF graph).

![all the proteins that interact with INSR_HUMAN (P06213)](./data/P06213-interactions.png "Interactome for INSR_HUMAN (P06213)")



## 1. Access data

You can either

- Use the Uniprot SPARQL endpoint [https://sparql.uniprot.org/sparql/](https://sparql.uniprot.org/sparql/)
    - pros:
        - you do not have to set up an endpoint
        - always up to date
    - cons:
        - you do not learn to set up an endpoint
        - requires an internet connection
        - the dataset is so big that it makes its exploration cumbersome
- Or set up your own endpoint with the data about your protein of interest (e.g. P06213)
    - pros:
        - can work offline
        - facilitates the exploration of the dataset (somehow)
        - you get to learn to set up your own endpoint
    - cons:
        - requires some tinkering
    - do not even try to download uniprot :-)
    - retrieve the data either
        - from the web page, link "Download" at the top of the page
        - from the URL: add the "`.rdf`" suffix to the URL `http://uniprot.org/uniprot/P06213` and download [http://uniprot.org/uniprot/P06213.rdf](http://uniprot.org/uniprot/P06213.rdf)
    - a local copy of the file is available from the `data/` directory as `data/P06213.rdf`
    - convert the RDF file from the RDF/XML format to turtle (optional but recommended, it will make the reading easier): `rapper -i rdfxml -o turtle P06213.rdf > P06213.ttl`
    - a local copy of the file is available from the `data/` directory as `data/P06213.ttl`
    - with [Apache fuseki]()
        - see [https://gitlab.com/odameron/rdf-sparql-cheatsheet](https://gitlab.com/odameron/rdf-sparql-cheatsheet)
        - download `apache-jena-fuseki-X.Y.Z.tar.gz` from [https://jena.apache.org/download/](https://jena.apache.org/download/)
        - in a terminal:
            - (optional) declare a `FUSEKI_HOME` variable: `export FUSEKI_HOME=/path/to/apache-jena-fuseki-X.Y.Z/`
            - start fuseki (no trailing `&` and leave the terminal open): `${FUSEKI_HOME}/fuseki-server --file=/path/to/data/P06213.ttl /uniprot`
        - in a web browser, open [http://localhost:3030](http://localhost:3030)



## 2. Retrieve the interaction graph

### 2.1 the proteins that interact with the protein of interest

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>

PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX up:<http://purl.uniprot.org/core/>

SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  ?protein up:interaction ?interaction .
  ?interaction rdf:type up:Non_Self_Interaction .
  ?otherProtein up:interaction ?interaction .
  FILTER (?protein != ?otherProtein)
  ?interaction up:experiments ?nbExperiments .

  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . # we force ?protein and ?otherProtein 
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic 
```



### 2.2 handle the case where the protein of interest interacts with itself

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>

PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX up:<http://purl.uniprot.org/core/>

SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  {
    ?protein up:interaction ?interaction .
    ?interaction rdf:type up:Non_Self_Interaction .
    ?otherProtein up:interaction ?interaction .
    FILTER (?protein != ?otherProtein)
    ?interaction up:experiments ?nbExperiments .
  }
  UNION
  {
    ?protein up:interaction ?interaction .
    ?interaction rdf:type up:Self_Interaction .
    ?otherProtein up:interaction ?interaction .
    FILTER (?protein = ?otherProtein)
    ?interaction up:experiments ?nbExperiments .
  }

  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . # we force ?protein and ?otherProtein 
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic 
```


### 2.3 add interactions between the proteins that interact with the protein of interest

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>

PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX up:<http://purl.uniprot.org/core/>

SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?otherProtein2 ?otherProtein2Mnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  ?protein up:interaction ?interaction .
  ?interaction rdf:type up:Non_Self_Interaction .
  ?otherProtein up:interaction ?interaction .
  FILTER (?protein != ?otherProtein)
  #?interaction up:experiments ?nbExperiments .
  
  ?protein up:interaction ?interaction2 .
  ?interaction2 rdf:type up:Non_Self_Interaction .
  ?otherProtein2 up:interaction ?interaction2 .
  FILTER (?protein != ?otherProtein2)
  #?interaction2 up:experiments ?nbExperiments .
  
  ?otherProtein up:interaction ?crossInteraction .
  ?otherProtein2 up:interaction ?crossInteraction .
  ?crossInteraction rdf:type up:Non_Self_Interaction .
  FILTER (?otherProtein > ?otherProtein2)
  ?crossInteraction up:experiments ?nbExperiments .

  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . 
  ?otherProtein2 up:mnemonic ?otherProtein2Mnemonic .
  ?otherProtein2 up:organism ?proteinTaxon .  
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic ?otherProtein2Mnemonic
```


### 2.4 add self-interactions for the proteins that interact with the protein of interest

```
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX dcterms: <http://purl.org/dc/terms/>

PREFIX taxon: <http://purl.uniprot.org/taxonomy/>
PREFIX uniprot: <http://purl.uniprot.org/uniprot/>
PREFIX up:<http://purl.uniprot.org/core/>

SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?otherProtein2 ?otherProtein2Mnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  ?protein up:interaction ?interaction .
  ?interaction rdf:type up:Non_Self_Interaction .
  ?otherProtein up:interaction ?interaction .
  FILTER (?protein != ?otherProtein)
  #?interaction up:experiments ?nbExperiments .
  
  {
    ?protein up:interaction ?interaction2 .
    ?interaction2 rdf:type up:Non_Self_Interaction .
    ?otherProtein2 up:interaction ?interaction2 .
    FILTER (?protein != ?otherProtein2)
    #?interaction2 up:experiments ?nbExperiments .
  
    ?otherProtein up:interaction ?crossInteraction .
    ?otherProtein2 up:interaction ?crossInteraction .
    ?crossInteraction rdf:type up:Non_Self_Interaction .
    FILTER (?otherProtein > ?otherProtein2)
    ?crossInteraction up:experiments ?nbExperiments . 
  }
  UNION
  {
    ?otherProtein up:interaction ?selfInteraction .
    ?selfInteraction rdf:type up:Self_Interaction .
    ?otherProtein2 up:interaction ?selfInteraction .
    FILTER (?otherProtein = ?otherProtein2) .
    ?selfInteraction up:experiments ?nbExperiments .
  }
  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . 
  ?otherProtein2 up:mnemonic ?otherProtein2Mnemonic .
  ?otherProtein2 up:organism ?proteinTaxon . 
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic ?otherProtein2Mnemonic
```


## 3. Export the interaction graph

### 3.1 Export as a diagram

```python
G = nx.Graph()

query = """
SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  {
    ?protein up:interaction ?interaction .
    ?interaction rdf:type up:Non_Self_Interaction .
    ?otherProtein up:interaction ?interaction .
    FILTER (?protein != ?otherProtein)
    ?interaction up:experiments ?nbExperiments .
  }
  UNION
  {
    ?protein up:interaction ?interaction .
    ?interaction rdf:type up:Self_Interaction .
    ?otherProtein up:interaction ?interaction .
    FILTER (?protein = ?otherProtein)
    ?interaction up:experiments ?nbExperiments .
  }

  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . # we force ?protein and ?otherProtein 
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic 
"""

sparql = SPARQLWrapper2(endpointURL)
sparql.setReturnFormat(JSON)

sparql.setQuery(prefixes + query)

for result in sparql.query().bindings:
    #print(f"{result['proteinMnemonic'].value}, {result['otherProteinMnemonic'].value}")
    G.add_edge(result['proteinMnemonic'].value, result['otherProteinMnemonic'].value, nbExperiments=result['nbExperiments'].value)

nx.draw(G, with_labels=True)
plt.show()

```

![all the proteins that interact with INSR_HUMAN (P06213)](./figures/INSR_interactors_direct.png "Direct interactome for INSR_HUMAN (P06213)")


```python
query = """
SELECT DISTINCT ?protein ?proteinMnemonic ?proteinTaxon ?otherProtein ?otherProteinMnemonic ?otherProtein2 ?otherProtein2Mnemonic ?nbExperiments
WHERE {
  VALUES ?protein { uniprot:P06213 }

  ?protein up:interaction ?interaction .
  ?interaction rdf:type up:Non_Self_Interaction .
  ?otherProtein up:interaction ?interaction .
  FILTER (?protein != ?otherProtein)
  #?interaction up:experiments ?nbExperiments .
  
  ?protein up:interaction ?interaction2 .
  ?interaction2 rdf:type up:Non_Self_Interaction .
  ?otherProtein2 up:interaction ?interaction2 .
  FILTER (?protein != ?otherProtein2)
  #?interaction2 up:experiments ?nbExperiments .
  
  ?otherProtein up:interaction ?crossInteraction .
  ?otherProtein2 up:interaction ?crossInteraction .
  ?crossInteraction rdf:type up:Non_Self_Interaction .
  FILTER (?otherProtein > ?otherProtein2)
  ?crossInteraction up:experiments ?nbExperiments .

  ?protein up:mnemonic ?proteinMnemonic .
  ?protein up:organism ?proteinTaxon .
  ?otherProtein up:mnemonic ?otherProteinMnemonic .
  ?otherProtein up:organism ?proteinTaxon . 
  ?otherProtein2 up:mnemonic ?otherProtein2Mnemonic .
  ?otherProtein2 up:organism ?proteinTaxon .  
}
ORDER BY ?proteinMnemonic ?otherProteinMnemonic 
"""

sparql = SPARQLWrapper2(endpointURL)
sparql.setReturnFormat(JSON)

sparql.setQuery(prefixes + query)

for result in sparql.query().bindings:
    #print(f"{result['otherProteinMnemonic'].value}, {result['otherProtein2Mnemonic'].value}")
    G.add_edge(result['otherProteinMnemonic'].value, result['otherProtein2Mnemonic'].value, nbExperiments=result['nbExperiments'].value)

nx.draw(G, with_labels=True)
plt.show()
```

![all the proteins that interact with INSR_HUMAN (P06213), including cross-interactions](./figures/INSR_interactors_cross.png "Interactome for INSR_HUMAN (P06213)")


### 3.2 Export as a RDF graph


## Todo

- [ ] add jupyter notebook
- [ ] complete section 1 with the rdfCartographer code (including taxon)




